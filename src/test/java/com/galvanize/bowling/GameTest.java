package com.galvanize.bowling;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class GameTest {
    Game game;
    @BeforeEach
    public void setUp(){
        game = new Game();
    }
    @Test
    public void setScore(){
        game.setScore(10);
        assertEquals(10, game.getScore());
    }
    @Test
    public void testGetScore(){
        assertEquals(0, game.getScore());
    }
}