package com.galvanize.bowling;

public class Game {

    int score;

    public Game(){
        score = 0;
    }

    public void setScore(int points){
        score += points;
    }

    public int getScore(){
        return score;
    }
}
